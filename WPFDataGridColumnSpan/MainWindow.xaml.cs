﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPFDataGridColumnSpan
{
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        /**
         * @brief コンストラクタ
         */
        public MainWindow()
        {
            InitializeComponent();

            // データグリッドにカラムを10列追加します。
            for (int i = 0; i < 10; ++i)
            {
                AddColumn(i);
            }

            // string型の行を追加します。
            var stringRows = new string[10];
            stringRows[0] = "列0";
            stringRows[1] = "列1";
            stringRows[2] = "列2";

            // 列3～5をセル結合します。
            var multiSpanText = "結合した列3～5";
            stringRows[3] = multiSpanText;
            stringRows[4] = multiSpanText;
            stringRows[5] = multiSpanText;

            stringRows[6] = "列6";
            stringRows[7] = "列7";
            stringRows[8] = "列8";
            stringRows[9] = "列9";

            // int型の行を生成します。
            var intRows = new int[10];
            for (int i = 0; i < 10; i++)
            {
                intRows[i] = i;
            }

            // データグリッドに行を追加します。
            var rows = new List<object>();
            rows.Add(stringRows);
            rows.Add(intRows);
            dataGrid.ItemsSource = rows;
        }

        /**
         * @brief データグリッドにカラムを追加します。
         */
        void AddColumn(int index)
        {
            var column = new DataGridTextColumn();
            column.Header = $"項目{index}";
            column.Binding = new Binding("[" + index + "]");
            dataGrid.Columns.Add(column);
        }
    }
}
