﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace WPFDataGridColumnSpan
{
    class DataGridSpannedCellPanel : DataGridCellsPanel
    {
        protected override Size ArrangeOverride(Size arrangeSize)
        {
            if (DataContext is IEnumerable)
            {
                // 親クラスのメソッドを呼び出します。
                base.ArrangeOverride(arrangeSize);

                var data = ((IEnumerable)DataContext).Cast<Object>();
                double totalPreviousWidth = 0;
                double totalPos = 0;
                var columnSize = new List<int>();
                var dataCount = data.Count();
                for (int i = 0; i < dataCount; ++i)
                {
                    Object nextElement = null;
                    if (dataCount > i + 1)
                    {
                        nextElement = data.ElementAt(i + 1);
                    }

                    var uiElement = InternalChildren[i];
                    var element = data.ElementAt(i);
                    if (Object.ReferenceEquals(element, nextElement) && element != null)
                    {
                        totalPreviousWidth += uiElement.RenderSize.Width;
                        uiElement.Arrange(new Rect(new Point(0, 0), new Size(0, 0)));
                    }
                    else
                    {
                        if (totalPreviousWidth > 0)
                        {
                            var point = new Point(totalPos, 0);
                            var width = totalPreviousWidth + uiElement.RenderSize.Width;
                            var height = uiElement.RenderSize.Height;
                            var size = new Size(width, height);
                            var rect = new Rect(point, size);
                            uiElement.Arrange(rect);
                        }

                        totalPos += uiElement.RenderSize.Width;
                        totalPreviousWidth = 0;
                    }
                }
                return arrangeSize;
            }
            else
            {
                return base.ArrangeOverride(arrangeSize);
            }
        }
    }
}
